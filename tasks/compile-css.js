"use strict"

module.exports = function(gulp, plugins, options, errorHandler, argv, browserSync) {
  return function() {

    return gulp.src([options.paths.dev.assets.sass])
      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      .pipe(plugins.sass({
        includePaths: "bower_components",
        outputStyle: options.compress ? "compressed" : "expanded"
      }))

      // Append any missing vendor prefixes for CSS3 properties
      .pipe(plugins.autoprefixer({
          browsers: ["last 2 versions", "IE 9", "IE 10"],
          cascade: false
      }))

      // if flag --cms=matrix change background image sources
      .pipe(plugins.if((argv.cms === "matrix"), plugins.replace("../images/", "mysource_files/" )))

      // Uncss
      .pipe(plugins.if(options.uncss, plugins.uncss({
        html: options.deployment.uncss.html,
        ignore: options.deployment.uncss.ignore
      })))

      // If the --compress flag is true then compress the CSS
      .pipe(plugins.if(options.compress, plugins.cssnano({autoprefixer: false})))

      // Write the CSS files to disk
      .pipe(gulp.dest(options.paths.dist.assets.css))

      // Inject CSS if not distribute task
      .pipe(plugins.if(!options.distribute, browserSync.stream()))
  }
}
